package main

import (
	"fmt"
	kelleyRabbimqPool "gitee.com/tym_hmm/rabbitmq-pool-go"
	RabbitmqRoute "gitee.com/tym_hmm/rabbitmq-pool-router-path-go"
	"sync"
	"testing"
)

var host = "192.168.1.169"
var port = 5672
var user = "temptest"
var pwd = "test123456"

/**
测试切面
所有路由前都会执行
 */
type TestHandlAop struct {
	
}

func (t *TestHandlAop)BeforeHandle(c *RabbitmqRoute.TaskContext)  {
	
}

/**
注册消费者及路由处理
*/
func TestConsumer(t *testing.T) {

	consumer := RabbitmqRoute.NewTask(host, port, user, pwd)

	//设置加载的节点信息(暂时只支持单节点加入)
	consumer.SetHandleNode(&RabbitmqRoute.NodeInfo{
		NodeName:     "dataCenter",
		ExchangeName: "temp-test-exchange-name1",
		ExchangeType: kelleyRabbimqPool.EXCHANGE_TYPE_TOPIC,
		Route:        "temp-test-queue-name1",
		QueueName:    "test-data-center-queue-name1",
		VirtualHosts: "/temptest1",
		IsTry:        false,
		MaxReTry:     1,
		IsAutoAck: false,//是否自动确认消息
	})

	consumer.SetHandleAop(&TestHandlAop{})
	//注册加载的路由
	consumer.RouteRegister(func(engine *RabbitmqRoute.TaskEngine) {
		//fmt.Println(engine)
		engine.AddRoute("/test", func(c *RabbitmqRoute.TaskContext) {
			fmt.Println(c.Request.Data)
			//c.Request.Data
		})
		engine.AddRoute("/test/a", func(c *RabbitmqRoute.TaskContext) {
			//如果节点 IsAutoAck 开启了手动确认消息(未开启自动确认消息) 则需加入
			_=c.RetryClient.Ack()
			fmt.Println(c.Request.Data)
		})
	})
	err := consumer.Enter()
	if err != nil {
		fmt.Println(err)
	}
}

/**
生产者处理
 */
func TestProduct(t *testing.T) {
	var wg sync.WaitGroup
	exchangeName := "temp-test-exchange-name1"
	exchangeType := kelleyRabbimqPool.EXCHANGE_TYPE_TOPIC
	queueName := "test-data-center-queue-name1"
	routeKey := "temp-test-queue-name1"
	routePath := "/test/a"


	wg.Add(2)
	go func() {
		defer wg.Done()
		data:="这是一个数据test/a"
		//product:=RabbitmqRoute.NewProductClient(host, port, user, pwd)
		product:=RabbitmqRoute.NewProductClientVirtualHosts(host, port, user, pwd, "/temptest1")
		err:=product.Publish(exchangeName, exchangeType, queueName, routeKey, routePath, data)
		if err !=nil{
			fmt.Println(err)
		}

	}()

	go func() {
		defer wg.Done()
		routePatsh := "/testaa"
		datas:="这是一个数据test"
		//products:=RabbitmqRoute.NewProductClient(host, port, user, pwd)
		products:=RabbitmqRoute.NewProductClientVirtualHosts(host, port, user, pwd, "/temptest1")
		errs:=products.Publish(exchangeName, exchangeType, queueName, routeKey, routePatsh, datas)
		if errs !=nil{
			fmt.Println(errs)
		}
	}()

	wg.Wait()
}
